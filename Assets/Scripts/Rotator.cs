﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public int RotateSpeed = 2;

    private void Update()
    {
        transform.Rotate(new Vector2(180,0) * Time.deltaTime * RotateSpeed);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("CountText"))
        {
            Destroy(this.gameObject);
        
        }
    }
}
