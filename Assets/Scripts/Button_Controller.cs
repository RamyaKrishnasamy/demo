﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class Button_Controller : MonoBehaviour, IPointerDownHandler, IPointerUpHandler

{

    public float Timer;
    public bool isClicked;
    public Image buttonImage;
    public Sprite PressedSprite;
    public Sprite UnpressedSprite;
	public GameObject Panel;
    public GameObject Spawner;
    private int Coins;
    public Text countText;
    public GameObject CoinShot;
    private float nextCoin;
    public float Coinrate;

    public void Start()
    {
        Coins = 0;
        SetCountText();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isClicked = true;
        buttonImage.sprite = PressedSprite;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isClicked = false;
        Timer = 0;
        buttonImage.sprite = UnpressedSprite;
    }

    private void Update()
    {
         if (isClicked && Time.time > nextCoin)
        {
            nextCoin = Time.time + Coinrate;
            Instantiate(CoinShot, Spawner.transform.position, CoinShot.transform.rotation, Spawner.transform);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
     {
		//Debug.Log("button controler" + other.gameObject.name);
       
		if(other.gameObject.CompareTag("Coin"))
        {
			Coins = Coins + 1;
			SetCountText ();
        }
     }

    void SetCountText()
    {
        countText.text = "Coins : " + Coins.ToString();
    }
}